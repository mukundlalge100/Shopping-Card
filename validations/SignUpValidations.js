const validator = require("validator");

const isEmpty = require("../util/isEmpty");

module.exports = validateUserSignUpInput = data => {
  let errors = {};
  const email = validator.normalizeEmail(data.email);
  const password = data.password.trim();
  const mobileNumber = data.mobileNumber.trim();
  const confirmPassword = data.confirmPassword.trim();
  const userName = data.userName.trim();

  if (isEmpty(userName)) {
    errors.userNameIsNotValid = "userName field is required!";
  }
  if (!validator.isLength(userName, { min: 2, max: 30 })) {
    errors.userNameIsNotValid =
      "User Name must be in between 2 and 30 characters!";
  }
  if (isEmpty(mobileNumber)) {
    errors.mobileNumberIsNotValid = "userName field is required!";
  }
  if (!validator.isMobilePhone(mobileNumber, "en-IN")) {
    errors.mobileNumberIsNotValid = "Mobile Number is not valid";
  }
  if (isEmpty(email)) {
    errors.emailIsNotValid = "Email field is required!";
  }
  if (!validator.isEmail(email)) {
    errors.emailIsNotValid = "Email is not valid,please enter valid email!";
  }
  if (isEmpty(password)) {
    errors.passwordIsNotValid = "password field is required!";
  }
  if (!validator.isLength(password, { min: 6, max: 30 })) {
    errors.passwordIsNotValid =
      "password must be in between 6 and 30 characters!";
  }
  if (!validator.equals(password, confirmPassword)) {
    errors.passwordsAreNotMatch = "Passwords must match!";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
