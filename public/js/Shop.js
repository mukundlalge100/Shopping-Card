const deleteOrder = async button => {
  const orderId = button.parentNode.querySelector("[name=orderId]").value;
  const csrfToken = button.parentNode.querySelector("[name=_csrf]").value;
  const parentElement = button.closest(".Orders-Order");
  const isDelete = confirm("Are you sure about deleting Order?");
  if (isDelete) {
    try {
      const result = await fetch(`/orders/${orderId}`, {
        method: "DELETE",
        headers: {
          "csrf-token": csrfToken
        }
      });
      const data = await result.json();
      if (data) {
        parentElement.remove();
      }
    } catch (error) {
      console.log(error);
      alert("Deleting product failed!!");
    }
  }
};
