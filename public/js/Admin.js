const deleteProduct = async button => {
  const productId = button.parentNode.querySelector("[name=productId]").value;
  const csrfToken = button.parentNode.querySelector("[name=_csrf]").value;

  const article = button.closest("article");
  const isDelete = confirm("Are you sure about deleting product?");
  if (isDelete) {
    try {
      const result = await fetch(`/admin/products/${productId}`, {
        method: "DELETE",
        headers: {
          "csrf-token": csrfToken
        }
      });
      const data = await result.json();
      if (data) {
        article.remove();
      }
    } catch (error) {
      const err = error.json();
      alert(err.message);
    }
  }
};
