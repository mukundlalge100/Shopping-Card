const express = require("express");
const authController = require("../controllers/AuthControllers/AuthController");
const router = express.Router();
const isAuth = require("../middleware/RouteProtection");

// @ROUTE           => /login
// @REQUEST_TYPE    => GET
// @DESC            => RENDERING LOGIN PAGE ROUTE
// @ACCESS          => PUBLIC
router.get("/login", authController.getLogIn);

// @ROUTE           => /login
// @REQUEST_TYPE    => POST
// @DESC            => LOGIN POST REQUEST ROUTE
// @ACCESS          => PUBLIC
router.post("/login", authController.postLogIn);

// @ROUTE           => /signup
// @REQUEST_TYPE    => GET
// @DESC            => RENDERING SIGNUP PAGE ROUTE
// @ACCESS          => PUBLIC
router.get("/signup", authController.getSignUp);

// @ROUTE           => /signup
// @REQUEST_TYPE    => POST
// @DESC            => SIGNUP POST REQUEST ROUTE
// @ACCESS          => PUBLIC
router.post("/signup", authController.postSignUp);

// @ROUTE           => /reset-password
// @REQUEST_TYPE    => GET
// @DESC            => RENDERING RESET-PASSWORD PAGE ROUTE
// @ACCESS          => PUBLIC
router.get("/reset-password", authController.getPasswordReset);

// @ROUTE           => /reset-password
// @REQUEST_TYPE    => POST
// @DESC            => RESET-PASSWORD POST REQUEST ROUTE
// @ACCESS          => PUBLIC
router.post("/reset-password", authController.postPasswordReset);

// @ROUTE           => /reset-password/:token
// @REQUEST_TYPE    => GET
// @DESC            => RENDERING RESET PASSWORD  PAGE WITH TOKEN ROUTE
// @ACCESS          => PUBLIC
router.get("/reset-password/:token", authController.getNewPassword);

// @ROUTE           => /new-password
// @REQUEST_TYPE    => POST
// @DESC            => FOR RESETTING NEW PASSWORD POST REQUEST ROUTE
// @ACCESS          => PUBLIC
router.post("/new-password", authController.postNewPassword);

// @ROUTE           => /logout
// @REQUEST_TYPE    => POST
// @DESC            => LOGOUT POST REQUEST ROUTE
// @ACCESS          => PRIVATE
router.post("/logout", isAuth, authController.postLogOut);

module.exports = router;
