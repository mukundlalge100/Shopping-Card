const express = require("express");
const shopController = require("../controllers/ShopControllers/ShopController");
const router = express.Router();
const isAuth = require("../middleware/RouteProtection");

// @ROUTE           => /
// @REQUEST_TYPE    => GET
// @DESC            => RENDERING INDEX PAGE ROUTE
// @ACCESS          => PUBLIC
router.get("/", shopController.getIndex);

// @ROUTE           => /products
// @REQUEST_TYPE    => GET
// @DESC            => RENDERING PRODUCTS PAGE ROUTE
// @ACCESS          => PUBLIC
router.get("/products", shopController.getProducts);

// @ROUTE           => /products/:productId
// @REQUEST_TYPE    => GET
// @DESC            => RENDERING PRODUCT DETAILS PAGE ROUTE
// @ACCESS          => PUBLIC
router.get("/products/:productId", shopController.getProduct);

// @ROUTE           => /cart
// @REQUEST_TYPE    => POST
// @DESC            => ADDING PRODUCT TO CART POST  REQUEST ROUTE
// @ACCESS          => PRIVATE
router.post("/cart", isAuth, shopController.postCart);

// @ROUTE           => /cart-delete-item
// @REQUEST_TYPE    => POST
// @DESC            => DELETING PRODUCT FROM CART POST REQUEST ROUTE
// @ACCESS          => PRIVATE
router.post("/cart-delete-item", isAuth, shopController.postCartDeleteProduct);

// @ROUTE           => /cart
// @REQUEST_TYPE    => GET
// @DESC            => RENDERING CART PAGE ROUTE
// @ACCESS          => PRIVATE
router.get("/cart", isAuth, shopController.getCart);

// @ROUTE           => /create-order
// @REQUEST_TYPE    => POST
// @DESC            => CREATING ORDER POST REQUEST ROUTE
// @ACCESS          => PRIVATE
router.post("/create-order", isAuth, shopController.postOrder);

// @ROUTE           => /delete-order
// @REQUEST_TYPE    => POST
// @DESC            => DELETING ORDER POST REQUEST ROUTE
// @ACCESS          => PRIVATE
router.delete("/orders/:orderId", isAuth, shopController.deleteOrder);

// @ROUTE           => /orders
// @REQUEST_TYPE    => GET
// @DESC            => RENDERING ORDERS PAGE ROUTE
// @ACCESS          => PRIVATE
router.get("/orders", isAuth, shopController.getOrders);

// @ROUTE           => /orders/:orderId
// @REQUEST_TYPE    => GET
// @DESC            => DOWNLOAD INVOICE FOR USER ...
// @ACCESS          => PRIVATE
router.get("/orders/:orderId", isAuth, shopController.getInvoice);

module.exports = router;
