const express = require("express");
const adminController = require("../controllers/AdminControllers/AdminController");
const router = express.Router();
const isAuth = require("../middleware/RouteProtection");

// @ROUTE           => /admin/add-product
// @REQUEST_TYPE    => GET
// @DESC            => RENDERING ADD PRODUCT PAGE ROUTE ...
// @ACCESS          => PRIVATE
router.get("/add-product", isAuth, adminController.getAddProductPage);

// @ROUTE           => /admin/add-product
// @REQUEST_TYPE    => POST
// @DESC            => ADD PRODUCT POST REQUEST ROUTE
// @ACCESS          => PRIVATE
router.post("/add-product", isAuth, adminController.postAddProduct);

// @ROUTE           => /admin/delete-product
// @REQUEST_TYPE    => POST
// @DESC            => DELETE PRODUCT POST REQUEST ROUTE
// @ACCESS          => PRIVATE
router.delete("/products/:productId", isAuth, adminController.deleteProduct);

// @ROUTE           => /admin/edit-product
// @REQUEST_TYPE    => POST
// @DESC            => EDIT PRODUCT POST REQUEST ROUTE
// @ACCESS          => PRIVATE
router.post("/edit-product", isAuth, adminController.postEditProduct);

// @ROUTE           => /admin/edit-product/:productId
// @REQUEST_TYPE    => GET
// @DESC            => RENDERING EDIT PRODUCT PAGE ROUTE
// @ACCESS          => PRIVATE
router.get("/edit-product/:productId", isAuth, adminController.getEditProduct);

// @ROUTE           => /admin/products
// @REQUEST_TYPE    => GET
// @DESC            => RENDERING PRODUCTS PAGE ROUTE
// @ACCESS          => PRIVATE
router.get("/products", isAuth, adminController.getProductList);

module.exports = router;
