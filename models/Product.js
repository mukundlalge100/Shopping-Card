const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const ProductScema = new Schema({
  title: {
    type: String,
    required: true
  },
  imageUrl: {
    type: String,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  userId: {
    type: Schema.Types.ObjectId,
    ref: "User",
    required: true
  }
});

module.exports = mongoose.model("Product", ProductScema);

// const getDB = require("../util/Database").getDB;
// const mongodb = require("mongodb");

// class Product {
//   constructor(title, imageUrl, price, description, id, userId) {
//     this.title = title;
//     this.imageUrl = imageUrl;
//     this.price = price;
//     this.description = description;
//     this._id = id ? new mongodb.ObjectID(id) : null;
//     this.userId = userId ? new mongodb.ObjectID(userId) : null;
//   }
//   async saveProduct() {
//     try {
//       const db = getDB();
//       if (this._id) {
//         // UPDATE PRODUCT...
//         const result = await db
//           .collection("products")
//           .updateOne({ _id: this._id }, { $set: this });
//       } else {
//         const result = await db.collection("products").insertOne(this);
//       }
//     } catch (error) {
//       console.log(error);
//     }
//   }
//   async fetchAllProDucts() {
//     try {
//       const db = getDB();
//       const products = await db
//         .collection("products")
//         .find()
//         .toArray();
//       return products;
//     } catch (error) {
//       console.log(error);
//     }
//   }
//   async getProductById(id) {
//     try {
//       const db = getDB();
//       id = new mongodb.ObjectID(id);
//       const product = await db.collection("products").findOne({ _id: id });
//       return product;
//     } catch (error) {
//       console.log(error);
//     }
//   }
//   async deleteProduct(id) {
//     try {
//       const db = getDB();
//       id = new mongodb.ObjectID(id);
//       const result = await db.collection("products").deleteOne({ _id: id });
//     } catch (error) {}
//   }
// }
// module.exports = Product;
