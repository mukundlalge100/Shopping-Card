const mongoose = require("mongoose");
const Order = require("./Order");
const Schema = mongoose.Schema;

const UserScema = new Schema({
  userName: {
    type: String,
    required: true
  },
  mobileNumber: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  cart: {
    items: [
      {
        productId: {
          type: Schema.Types.ObjectId,
          ref: "Product",
          required: true
        },
        quantity: { type: Number, required: true }
      }
    ]
  },
  resetToken: {
    type: String
  },
  resetTokenExpiration: {
    type: Date
  }
});

UserScema.methods.addToCart = async function(product) {
  const productCartIndex = this.cart.items.findIndex(cp => {
    return cp.productId.toString() === product._id.toString();
  });
  const updatedCartItems = [...this.cart.items];
  let newQuantity = 1;
  if (productCartIndex >= 0) {
    newQuantity = updatedCartItems[productCartIndex].quantity + 1;
    updatedCartItems[productCartIndex].quantity = newQuantity;
  } else {
    updatedCartItems.push({
      productId: product._id,
      quantity: newQuantity
    });
  }
  const updatedCart = { items: updatedCartItems };
  this.cart = updatedCart;
  try {
    await this.save();
  } catch (error) {
    const err = new Error(error);
    err.httpStatusCode = 505;
    return next(err);
  }
};
UserScema.methods.deleteProductFromCart = async function(productId) {
  try {
    const updatedCartItems = this.cart.items.filter(item => {
      return item.productId.toString() !== productId.toString();
    });
    this.cart.items = updatedCartItems;
    await this.save();
  } catch (error) {
    const err = new Error(error);
    err.httpStatusCode = 505;
    return next(err);
  }
};
UserScema.methods.clearCart = async function() {
  this.cart.items = [];
  await this.save();
};
module.exports = mongoose.model("User", UserScema);

// const mongodb = require("mongodb");
// const getDB = require("../util/Database").getDB;
// class User {
//   constructor(name, email, cart, userId) {
//     this.name = name;
//     this.email = email;
//     this.cart = cart;
//     this._id = userId ? new mongodb.ObjectID(userId) : null;
//   }
//   async registerUser() {
//     try {
//       const db = getDB();
//       const result = await db.collection("users").insertOne(this);
//     } catch (error) {
//       console.log(error);
//     }
//   }
//   async addToCart(product) {
//     const productCartIndex = this.cart.items.findIndex(cp => {
//       return cp.productId.toString() === product._id.toString();
//     });
//     const updatedCartItems = [...this.cart.items];
//     let newQuantity = 1;
//     if (productCartIndex >= 0) {
//       newQuantity = updatedCartItems[productCartIndex].quantity + 1;
//       updatedCartItems[productCartIndex].quantity = newQuantity;
//     } else {
//       updatedCartItems.push({
//         productId: new mongodb.ObjectID(product._id),
//         quantity: newQuantity
//       });
//     }

//     const updatedCart = { items: updatedCartItems };
//     const db = getDB();
//     try {
//       await db
//         .collection("users")
//         .updateOne({ _id: this._id }, { $set: { cart: updatedCart } });
//     } catch (error) {
//       console.log(error);
//     }
//   }
//   async getCart() {
//     const db = getDB();
//     const productIds = this.cart.items.map(item => {
//       return item.productId;
//     });
//     let products = await db
//       .collection("products")
//       .find({ _id: { $in: productIds } })
//       .toArray();
//     products = products.map(product => {
//       return {
//         ...product,
//         quantity: this.cart.items.find(item => {
//           return item.productId.toString() === product._id.toString();
//         }).quantity
//       };
//     });
//     return products;
//   }
//   async findUserById(userId) {
//     try {
//       const db = getDB();
//       userId = new mongodb.ObjectID(userId);
//       const user = await db.collection("users").findOne({ _id: userId });
//       return user;
//     } catch (error) {
//       console.log(error);
//     }
//   }
//   async deleteProductFromCart(productId) {
//     try {
//       const updatedCartItems = this.cart.items.filter(item => {
//         return item.productId.toString() !== productId.toString();
//       });
//       const db = getDB();
//       db.collection("users").updateOne(
//         { _id: this._id },
//         { $set: { cart: { items: updatedCartItems } } }
//       );
//     } catch (error) {
//       console.log(error);
//     }
//   }
//   async addOrder() {
//     try {
//       const db = getDB();
//       const products = await this.getCart();
//       let totalPrice = 0;
//       products.forEach(product => {
//         totalPrice = product.quantity * product.price + totalPrice;
//       });
//       const order = {
//         products: products,
//         user: {
//           _id: this._id,
//           name: this.name,
//           email: this.email
//         },
//         totalPrice: totalPrice
//       };
//       await db.collection("orders").insertOne(order);
//       this.cart = { items: [] };
//       await db
//         .collection("users")
//         .updateOne({ _id: this._id }, { $set: { cart: { items: [] } } });
//     } catch (error) {
//       console.log(error);
//     }
//   }
//   async getOrders() {
//     const db = getDB();
//     try {
//       const orders = await db
//         .collection("orders")
//         .find({ "user._id": this._id })
//         .toArray();
//       return orders;
//     } catch (error) {
//       console.log(error);
//     }
//   }
// }

// module.exports = User;
