const Product = require("../models/Product");

const getPagination = async request => {
  const ITEM_PER_PAGE = 3;
  const page = +request.query.page || 1;
  const start = (page - 1) * ITEM_PER_PAGE;
  const end = page * ITEM_PER_PAGE;
  const totalProducts = await Product.find().countDocuments();
  const totalPages = Math.ceil(totalProducts / ITEM_PER_PAGE);

  return {
    ITEM_PER_PAGE,
    page,
    start,
    end,
    totalProducts,
    totalPages
  };
};
module.exports = getPagination;
