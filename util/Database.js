const mongodb = require("mongodb");

const MongoClient = mongodb.MongoClient;
let _db;
const mongoConnect = async callback => {
  try {
    const client = await MongoClient.connect(
      "mongodb+srv://mukund:lms8999710@cluster0-253k8.mongodb.net/shop?retryWrites=true",
      { useNewUrlParser: true }
    );
    _db = client.db("shop");
    callback();
  } catch (error) {
    console.log(error);
  }
};
const getDB = () => {
  if (_db) {
    return _db;
  }
  throw "No database is connected!";
};
exports.mongoConnect = mongoConnect;
exports.getDB = getDB;
