// GLOBAL IMPORTS FOR MODULES...
const express = require("express");
const path = require("path");
const app = express();
const bodyParser = require("body-parser");
const multer = require("multer");

const mongoose = require("mongoose");
const MONGODB_URL =
  "mongodb+srv://mukund:lms8999710@cluster0-253k8.mongodb.net/shop?retryWrites=true";

// CONFIG FOR SESSIONS SO IT WILL AVAILABLE IN REQUEST...
const session = require("express-session");
const MongoDbStore = require("connect-mongodb-session")(session);
const store = new MongoDbStore({
  uri: MONGODB_URL,
  collection: "sessions"
});

const User = require("./models/User");

const sassMiddleware = require("node-sass-middleware");

const csrf = require("csurf");

const flash = require("connect-flash");

const adminRoutes = require("./routes/Admin");
const shopRoutes = require("./routes/Shop");
const authRoutes = require("./routes/Auth");

const pageNotFoundController = require("./controllers/ErrorControllers/404Controller");
const get505Error = require("./controllers/ErrorControllers/505Controller");

const rootDir = require("./util/Path");

// CONFIG FOR BODY PARSER SO BODY OF REQUEST WILL BE AVAILABLE...
app.use(bodyParser.urlencoded({ extended: false }));

// MULTER FILE UPLOADER CONFIG, MULTIPART FORM CONFIG ...
const fileStorage = multer.diskStorage({
  destination: (request, file, cb) => {
    cb(null, "assets/images");
  },
  filename: (request, file, cb) => {
    cb(null, new Date().getTime() + file.originalname);
  }
});
const fileFilter = (request, file, cb) => {
  if (
    file.mimetype === "image/png" ||
    file.mimetype === "image/jpg" ||
    file.mimetype === "image/jpeg"
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};
app.use(
  multer({ storage: fileStorage, fileFilter: fileFilter }).single("imageUrl")
);

// CONNECTION TO DB...
mongoose
  .connect(MONGODB_URL, { useNewUrlParser: true })
  .then(result => {
    console.log("Server is running on port 5000!\nMongoDB Connected!");
    app.listen(5000);
  })
  .catch(error => {
    console.log(error);
  });

// SESSION MIDDLEWARE...
app.use(
  session({
    secret: "12345678",
    resave: false,
    saveUninitialized: false,
    store: store
  })
);

// STORING USER IN ALL REQUEST IF USER IS IN SESSION...
app.use(async (request, response, next) => {
  if (!request.session.user) {
    return next();
  }
  try {
    const user = await User.findById(request.session.user._id);
    request.user = user;
    next();
  } catch (error) {
    console.log(error);
  }
});

// CONFIGURING SCSS SETUP IN PROJECT...
const srcPath = rootDir + "/sass";
const destPath = rootDir + "/public/css";
app.use(
  "/css",
  sassMiddleware({
    src: srcPath,
    dest: destPath,
    debug: false,
    outputStyle: "compressed"
  })
);

// CSRF (CROSS SIDE REQUEST FORGERY) TOKEN CONFIGURATION...
const csrfProtection = csrf();
app.use(csrfProtection);
app.use((request, response, next) => {
  response.locals.isAuthenticated = request.session.isLoggedIn;
  response.locals.csrfToken = request.csrfToken();
  next();
});

// CONNECT-FLASH CONFIG...
app.use(flash());

// ALL ROUTES OF APP ...
app.use("/admin", adminRoutes);
app.use(shopRoutes);
app.use(authRoutes);

// TO SET VIEWS FOR APP USING EJS TEMPLATE ENGINE...
app.set("view engine", "ejs");
app.set("views", "views");

// FOR STATIC FOLDER LIKE IMAGES AND STYLES FILES...
app.use("/assets", express.static(path.join(rootDir, "assets")));
app.use(express.static(path.join(rootDir, "public")));

// PAGE NOT FOUND MIDDLEWARE ...
app.get("/505", get505Error.get505Error);
app.use((error, request, response, next) => {
  response.render("505", {
    pageTitle: "Error!",
    path: "/error",
    error: error
  });
});
app.use(pageNotFoundController.get404Error);
