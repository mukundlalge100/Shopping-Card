const User = require("../../models/User");
const bcrypt = require("bcryptjs");
const crypto = require("crypto");
const validateUserSignUpInput = require("../../validations/SignUpValidations");
const validateUserLogInInput = require("../../validations/LogInValidations");

// SENDGRID NODEMAILER CONFIG...
const nodeMailer = require("nodemailer");
const sendGridTransport = require("nodemailer-sendgrid-transport");

try {
  var transportor = nodeMailer.createTransport(
    sendGridTransport({
      auth: {
        api_key:
          "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"
      }
    })
  );
} catch (error) {
  console.log(error);
}

// GET REQUEST FOR LOGIN PAGE ...
exports.getLogIn = (request, response, next) => {
  response.render("Auth/LogIn", {
    path: "/login",
    pageTitle: "LogIn Page!",
    flashErrors: request.flash("error"),
    errors: {},
    oldInput: {}
  });
};

// POST REQUEST FOR USER LOGIN ...
exports.postLogIn = async (request, response, next) => {
  try {
    const password = request.body.password;
    const email = request.body.email;
    // CHECKING VALIDATION ALL USER INPUT FIELDS...
    const { errors, isValid } = validateUserLogInInput(request.body);
    if (!isValid) {
      return response.status(420).render("Auth/LogIn", {
        path: "/login",
        pageTitle: "LogIn Page!",
        errors: errors,
        flashErrors: request.flash("error"),
        oldInput: { email }
      });
    }
    // CHECKING IF USER EXIST OR NOT IN DB...
    const user = await User.findOne({ email: email });
    if (!user) {
      request.flash("error", "Email is not exists,Please enter valid email!");
      return response.status(420).render("Auth/LogIn", {
        path: "/login",
        pageTitle: "LogIn Page!",
        errors: {},
        flashErrors: request.flash("error"),
        oldInput: { email }
      });
    }
    // CHECK IF PASSWORD IS VALID OR NOT ...
    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) {
      request.flash(
        "error",
        "Password credentials are not valid,please enter valid credentials!"
      );
      return response.status(420).render("Auth/LogIn", {
        path: "/login",
        pageTitle: "LogIn Page!",
        errors: {},
        flashErrors: request.flash("error"),
        oldInput: { email }
      });
    }
    // IF VALIDATIONS SUCCESSFUL ,ADD USER IN SESSIONS AND LOGGED IN USER...
    request.session.user = user;
    request.session.isLoggedIn = true;
    request.session.save(error => {
      if (error) {
        const err = new Error(error);
        err.httpStatusCode = 505;
        return next(err);
      }
      // AFTER LOGGED IN REDIRECT USER TO HOME PAGE ...
      response.redirect("/");
    });
  } catch (error) {
    const err = new Error(error);
    err.httpStatusCode = 505;
    return next(err);
  }
};

// GET REQUEST FOR USER SIGNUP PAGE ...
exports.getSignUp = (request, response, next) => {
  response.render("Auth/SignUp", {
    path: "/signup",
    pageTitle: "SignUp Page!",
    flashErrors: request.flash("error"),
    errors: {},
    oldInput: {}
  });
};

// POST REQUEST FOR REGISTERING USER IN OUR APP...
exports.postSignUp = async (request, response, next) => {
  const userName = request.body.userName;
  const mobileNumber = request.body.mobileNumber;
  const email = request.body.email;
  const password = request.body.password;

  // CHECKING VALIDATION ALL USER INPUT FIELDS...
  const { errors, isValid } = validateUserSignUpInput(request.body);
  if (!isValid) {
    return response.status(420).render("Auth/SignUp", {
      path: "/signup",
      pageTitle: "SignUp Page!",
      errors: errors,
      flashErrors: request.flash("error"),
      oldInput: { userName, email, mobileNumber }
    });
  }

  try {
    // CHECK IF USER IS ALREADY EXIST OR NOT ...
    const userExist = await User.findOne({ email: email }).select("email -_id");

    if (userExist) {
      // IF USER IS ALREADY EXISTS,THEN REDIRECT USER TO SIGN UP PAGE WITH ERROR MESSAGES ...
      await request.flash(
        "error",
        "Email is already exists! Please try different email."
      );
      return response.status(420).render("Auth/SignUp", {
        path: "/signup",
        pageTitle: "SignUp Page!",
        errors: errors,
        flashErrors: request.flash("error"),
        oldInput: { userName, mobileNumber }
      });
    }

    // ENCRYPT PASSWORD BEFORE STORING IT INTO DATABASE...
    const bcryptPassword = await bcrypt.hash(password, 16);

    const user = new User({
      userName,
      mobileNumber,
      email,
      password: bcryptPassword,
      cart: { items: [] }
    });
    // AFTER VALIDATIONS SUCCESSFUL,SAVE USER IN DATABASE ...
    await user.save();

    // AFTER SUCCESSFUL SIGNUP, SEND EMAIL TO USER FOR SUCCESSFUL SIGNED UP USING SENDGRID AND NODEMAILER ...
    transportor.sendMail({
      to: email,
      from: "mukundlalge100@gmail.com",
      subject: "Congratulations,sign up succeeded!",
      html:
        "<h1>You have successfully signed up!!</h1> <br><h2>Welcome to ShopiFier !</h2>"
    });

    // AFTER SIGNED UP REDIRECT USER TO LOGIN PAGE TO LOGIN AGAIN ...
    response.redirect("/login");
  } catch (error) {
    const err = new Error(error);
    err.httpStatusCode = 505;
    return next(err);
  }
};

// GET REQUEST FOR PASSWORDRESET PAGE ...
exports.getPasswordReset = (request, response, next) => {
  response.render("Auth/PasswordReset", {
    path: "/reset-password",
    pageTitle: "Reset Password Page!",
    errorMessage: request.flash("error")
  });
};

// POST REQUEST FOR RESSETING USER PASSWORD ...
exports.postPasswordReset = async (request, response, next) => {
  // GENERATING TOKEN USING CRYPTO MODULE ...
  const token = crypto.randomBytes(32).toString("hex");

  const email = request.body.email;

  try {
    // IF USER IS ALREADY EXISTS,THEN REDIRECT USER TO RESET PASSWORD PAGE WITH ERROR MESSAGES ...
    const user = await User.findOne({ email: email });
    if (!user) {
      request.flash("error", "Email is not exists!");
      response.redirect("/reset-password");
    }
    user.resetToken = token;
    user.resetTokenExpiration = Date.now() + 3600000;
    // SAVE TOKEN AND TOKEN EXPIRATION TIME AFTER USER REQUEST FOR RESETING PASSWORD...
    await user.save();

    // AFTER SAVING TOKEN SEND USER EMAIL LINK FOR CHANGING PASSWORD...
    transportor.sendMail({
      to: email,
      from: "mukundlalge100@gmail.com",
      subject: "Reset your password",
      html: `<h2>Request for resetting your password</h2>
        <p>Click here <a href = http://localhost:5000/reset-password/${token}>to set new password</a>.</p>`
    });
    // REDIRECT USER AFTER SAVING AND SENDING EMAIL TO USER...
    response.redirect("/");
  } catch (error) {
    const err = new Error(error);
    err.httpStatusCode = 505;
    return next(err);
  }
};

// GET REQUEST FOR USER NEW PASSWORD PAGE WHICH HAS TOKEN...
exports.getNewPassword = async (request, response, next) => {
  const token = request.params.token;
  try {
    // FIND USER WITH TOKEN IF IT IS VALID AND NOT EXPIRED...
    const user = await User.findOne({
      resetToken: token,
      resetTokenExpiration: { $gt: Date.now() }
    }).select("_id");

    if (!user) {
      // IF TOKEN IS INVALID OR EXPIRED REDIRECT USER TO RESET PASSWORD PAGE...
      request.flash("error", "Invalid token or token has expired!");
      return response.redirect("/reset-password");
    }
    // IF TOKEN IS VALID RENDER NEW PASSWORD PAGE...
    response.render("Auth/NewPassword", {
      path: "/new-password",
      pageTitle: "New Password Page!",
      errorMessage: request.flash("error"),
      userId: user._id.toString(),
      token: token
    });
  } catch (error) {
    console.log(error);
  }
};

// POST REQUEST FOR CHANGING USER PASSWORD...
exports.postNewPassword = async (request, response, next) => {
  const password = request.body.password;
  const userId = request.body.userId;
  const token = request.body.token;
  let resetUser;

  try {
    // IF USER HAS VALID TOKEN OR NOT ...
    const user = await User.findOne({
      _id: userId,
      resetToken: token,
      resetTokenExpiration: { $gt: Date.now() }
    });

    if (!user) {
      // IF TOKEN IS INVALID OR EXPIRED REDIRECT USER TO RESET PASSWORD PAGE...
      request.flash("error", "Invalid token or token has expired!");
      return response.redirect("/reset-password");
    }
    // ENCRYPT PASSWORD BEFORE STORING IT INTO DATABASE...
    const bcryptPassword = await bcrypt.hash(password, 16);

    resetUser = user;
    resetUser.password = bcryptPassword;

    // REMOVING USER TOKEN FROM DATABASE AFTER SETTING NEW PASSWORD...
    resetUser.resetToken = undefined;
    resetUser.resetTokenExpiration = undefined;
    await resetUser.save();

    // AFTER SUCCESSFULLY RESETTING NEW PASSWORD, SEND EMAIL TO USER FOR SUCCESS MESSAGE ...
    transportor.sendMail({
      to: user.email,
      from: "mukundlalge100@gmail.com",
      subject: "Congratulations,reset successfully!",
      html: "<h1>You have successfully change your password!</h1>"
    });

    // REDIRECT USER AFTER SETTING NEW PASSWORD ...
    response.redirect("/login");
  } catch (error) {
    const err = new Error(error);
    err.httpStatusCode = 505;
    return next(err);
  }
};
exports.postLogOut = (request, response, next) => {
  request.session.destroy(error => {
    if (error) {
      const err = new Error(error);
      err.httpStatusCode = 505;
      return next(err);
    }
    response.redirect("/");
  });
};
