const Product = require("../../models/Product");
const file = require("../../util/File");
const getPagination = require("../../util/Pagination");

//RENDER GET EDITPRODUCT PAGE...
exports.getAddProductPage = (request, response, next) => {
  response.render("Admin/EditProduct", {
    path: "/admin/add-product",
    pageTitle: "Add Product!",
    editing: false
  });
};

// POST ADDPRODUCT REQUEST...
exports.postAddProduct = async (request, response, next) => {
  try {
    const title = request.body.title;
    const imageUrl = request.file.path;
    const price = request.body.price;
    const description = request.body.description;
    const product = new Product({
      title,
      imageUrl,
      price,
      description,
      userId: request.user._id
    });

    await product.save();
    response.redirect("/products");
  } catch (error) {
    console.log(error);
    const err = new Error(error);
    err.httpStatusCode = 505;
    return next(err);
  }
};

// GET REQUEST FOR RENDERING ADMIN PRODUCTS...
exports.getProductList = async (request, response, next) => {
  try {
    const products = await Product.find({ userId: request.user._id }).select(
      "title price imageUrl description"
    );
    response.render("Admin/AdminProductList", {
      pageTitle: "Admin Products!",
      products: products,
      path: "/admin/products"
    });
  } catch (error) {
    console.log(error);
    const err = new Error(error);
    err.httpStatusCode = 505;
    return next(err);
  }
};

// GET REQUEST FOR EDIT PRODUCT PAGE...
exports.getEditProduct = async (request, response, next) => {
  const editMode = request.query.edit;
  const productId = request.params.productId;
  try {
    const prod = await Product.findById(productId).select(
      "title price imageUrl description"
    );
    response.render("Admin/EditProduct", {
      path: "/admin/edit-product",
      pageTitle: "Edit Product",
      editing: editMode,
      product: prod
    });
  } catch (error) {
    const err = new Error(error);
    err.httpStatusCode = 505;
    return next(err);
  }
};

// POST REQUEST FOR EDITING PRODUCT...
exports.postEditProduct = async (request, response, next) => {
  const title = request.body.title;
  const price = request.body.price;
  const description = request.body.description;
  const productId = request.body.productId;
  const image = request.file;
  try {
    const product = await Product.findOne({ _id: productId }).select(
      "userId imageUrl title price description"
    );
    if (!product) {
      return next(new Error("Product not found!!"));
    }
    if (product) {
      if (product.userId.toString() !== request.user._id.toString()) {
        return response.redirect("/");
      }
    }
    let imageUrl = product.imageUrl;

    // IF THERE IS IMAGE IN REQUEST DELETE PREVIOUS IMAGE AND CREATE NEW UPDATED IMAGE ...
    if (image) {
      file.deleteFile(product.imageUrl);
      imageUrl = image.path;
    }
    await Product.findByIdAndUpdate(
      { _id: productId },
      { title, imageUrl, price, description }
    );
    console.log("Product updated!");
    response.redirect("/admin/products");
  } catch (error) {
    const err = new Error(error);
    err.httpStatusCode = 505;
    return next(err);
  }
};

// POST REQUEST FOR DELETING PRODUCT ...
exports.deleteProduct = async (request, response, next) => {
  const productId = request.params.productId;
  try {
    const product = await Product.findOne({ _id: productId }).select(
      "userId imageUrl"
    );

    if (product) {
      if (product.userId.toString() !== request.user._id.toString()) {
        return response.redirect("/");
      }
    }
    // DELETE IMAGE FROM SERVER AFTER DELETING PRODUCT ...
    file.deleteFile(product.imageUrl);

    const result = await Product.findByIdAndDelete({ _id: productId });

    // DELETE PRODUCT FROM CART AS WELL ...
    request.user.deleteProductFromCart(productId);
    if (result) {
      console.log("product deleted successfully!!");
      response.status(200).json({ message: "Product Delete Successfully!!" });
    }
  } catch (error) {
    response.status(505).json({ message: "Deleting Product failed!!" });
  }
};
