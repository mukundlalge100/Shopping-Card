const Product = require("../../models/Product");
const Order = require("../../models/Order");
const path = require("path");
const fs = require("fs");
const pdfKit = require("pdfkit");
const file = require("../../util/File");
const getPagination = require("../../util/Pagination");

// RENDER PRODUCTS IN PRODUCTS PAGE ...

exports.getProducts = async (request, response, next) => {
  try {
    const pagination = await getPagination(request);
    const products = await Product.find()
      .skip(pagination.start)
      .limit(pagination.ITEM_PER_PAGE)
      .select("title price imageUrl description");
    response.render("Shop/ShopProductList", {
      pageTitle: "Products Page!",
      products: products,
      path: "/products",
      page: pagination.page,
      hasPreviousPage: pagination.page > 1,
      isLastPage: pagination.end >= pagination.totalProducts,
      isFirstPage: pagination.page === 1 && pagination.totalPages > 1
    });
  } catch (error) {
    const err = new Error(error);
    err.httpStatusCode = 505;
    return next(err);
  }
};

// GET PRODUCT DETAILS METHOD ...
exports.getProduct = async (request, response, next) => {
  const id = request.params.productId;
  try {
    const prod = await Product.findById(id).select(
      "title price imageUrl description"
    );
    response.render("Shop/ProductDetails", {
      product: prod,
      path: "/products",
      pageTitle: "Product Details"
    });
  } catch (error) {
    const err = new Error(error);
    err.httpStatusCode = 505;
    return next(err);
  }
};

// RENDER INDEX PAGE ...
exports.getIndex = async (request, response, next) => {
  try {
    const pagination = await getPagination(request);
    const products = await Product.find()
      .skip(pagination.start)
      .limit(pagination.ITEM_PER_PAGE)
      .select("title price imageUrl description");

    response.render("Shop/Index", {
      pageTitle: "Index Page!",
      products: products,
      path: "/",
      page: pagination.page,
      hasPreviousPage: pagination.page > 1,
      isLastPage: pagination.end >= pagination.totalProducts,
      isFirstPage: pagination.page === 1 && pagination.totalPages > 1
    });
  } catch (error) {
    const err = new Error(error);
    err.httpStatusCode = 505;
    return next(err);
  }
};

// RENDER CART PAGE ...
exports.getCart = async (request, response, next) => {
  try {
    const user = await request.user
      .populate("cart.items.productId")
      .execPopulate();
    const products = user.cart.items;
    response.render("Shop/Cart", {
      path: "/cart",
      products: products,
      pageTitle: "Cart Page"
    });
  } catch (error) {
    const err = new Error(error);
    err.httpStatusCode = 505;
    return next(err);
  }
};

// ADD PRODUCT INTO CART POST METHOD ...
exports.postCart = async (request, response, next) => {
  const productId = request.body.productId;
  try {
    const product = await Product.findById(productId).select(
      "title price imageUrl"
    );
    await request.user.addToCart(product);
    response.redirect("/cart");
  } catch (error) {
    const err = new Error(error);
    err.httpStatusCode = 505;
    return next(err);
  }
};

// DELETE PRODUCT FROM CART ...
exports.postCartDeleteProduct = async (request, response, next) => {
  const productId = request.body.productId;
  try {
    await request.user.deleteProductFromCart(productId);
    response.redirect("/cart");
  } catch (error) {
    const err = new Error(error);
    err.httpStatusCode = 505;
    return next(err);
  }
};

// POST ORDER FOR CART PRODUCTS FOR AUTHENTICATED USERS AND CLEAR CART PRODUCTS AFTER PLACING ORDER...
exports.postOrder = async (request, response, next) => {
  try {
    const user = {
      userId: request.user._id,
      userName: request.user.userName,
      mobileNumber: request.user.mobileNumber,
      email: request.user.email
    };

    let userTemp = await request.user
      .populate("cart.items.productId")
      .execPopulate();

    const productsRaw = userTemp.cart.items;

    const products = productsRaw.map(product => {
      return {
        quantity: product.quantity,
        product: { ...product.productId._doc }
      };
    });

    let totalPrice = 0;
    products.forEach(product => {
      totalPrice = product.quantity * product.product.price + totalPrice;
    });
    const order = new Order({
      user,
      products,
      totalPrice
    });
    await order.save();
    await request.user.clearCart();
    response.redirect("/orders");
  } catch (error) {
    const err = new Error(error);
    err.httpStatusCode = 505;
    return next(err);
  }
};

// DELETE ORDER FROM UI AND DATABASE ...
exports.deleteOrder = async (request, response, next) => {
  const orderId = request.params.orderId;
  const invoiceName = "invoice-" + orderId + ".pdf";
  const invoicePath = path.join("data", "Invoices", invoiceName);
  try {
    const order = await Order.findByIdAndDelete(orderId);
    if (!order) {
      return next(new Error("Order not found!!"));
    }
    file.deleteFile(invoicePath);
    response.status(200).json({ message: "Product Delete SuccessFully!!" });
  } catch (error) {
    response.status(505).json({ message: "Deleting order failed!!" });
  }
};

// RENDERING ORDER PAGE ...
exports.getOrders = async (request, response, next) => {
  try {
    let orders = await Order.find().select("products totalPrice");
    response.render("Shop/Orders", {
      path: "/orders",
      pageTitle: "orders page",
      orders: orders
    });
  } catch (error) {
    const err = new Error(error);
    err.httpStatusCode = 505;
    return next(err);
  }
};
// DOWNLOADING PDF FILE FOR INVOICE ...
exports.getInvoice = async (request, response, next) => {
  const orderId = request.params.orderId;
  try {
    const order = await Order.findById(orderId);
    if (!order) {
      return next(new Error("No order found!!"));
    }
    if (order.user.userId.toString() !== request.user._id.toString()) {
      return next(new Error("Unautherized user!"));
    }
    const invoiceName = "invoice-" + orderId + ".pdf";

    const invoicePath = path.join("data", "Invoices", invoiceName);

    const pdfDocument = new pdfKit();

    pdfDocument.pipe(fs.createWriteStream(invoicePath));
    pdfDocument.pipe(response);

    pdfDocument
      .fontSize(26)
      .fillColor("#cc3366")
      .text("--------------------INVOICE--------------------");

    pdfDocument
      .fontSize(20)
      .fillColor("#cc3366")
      .text("\n\n #Product Details => ");

    order.products.forEach(product => {
      pdfDocument
        .fontSize(18)
        .text(
          `\n\nProduct Name => ${product.product.title} \n\nQuantity => ${
            product.quantity
          } \n\nActual Product Price => $${
            product.product.price
          }\n\nTotal Product Price => $${product.product.price *
            product.quantity}`
        )
        .fill("#cc3366");
    });

    pdfDocument
      .fontSize(26)
      .fillColor("#cc3366")
      .text(`\n\n\n-----Total Grand Price => $${order.totalPrice} -----`);

    response.setHeader("Content-Type", "application/pdf");
    response.setHeader(
      "Content-Disposition",
      `inline; filename = ${invoiceName}`
    );

    pdfDocument.end();
  } catch (error) {
    console.log(error);
    const err = new Error(error);
    return next(err);
  }
};
